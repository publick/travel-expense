import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor() { }

  arFromLocation = [
    {name: 'Arizona', abbrev: 'AZ'},
    {name: 'California', abbrev: 'CA'},
    {name: 'Colorado', abbrev: 'CO'},
    {name: 'New York', abbrev: 'NY'},
    {name: 'Pennsylvania', abbrev: 'PA'},
  ];
  arToLocation = [
    {name: 'Kolkata'},
    {name: 'Howrah'},
  ]

  arPurpuse = [
    {name: 'Business Travel'},
    {name: 'Normale Travel'},
  ]
  arTripFor = [
    {name: 'Self'},
    {name: 'Famaly'},
  ]

  arTripCostCenter = [
    {name: 'Normale'},
  ]
  arEventCode = [
    {name: 'CD584'},
    {name: 'CS23'},
  ]
}
