import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule }    from '@angular/forms';
import { CreateTripComponent } from './creat/create-trip.component';
import { TripRoutingModule } from './trip-routing.module'
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatStepperModule } from '@angular/material/stepper';

import { TripInformationComponent } from './creat/trip-information/trip-information.component';
import { TravelDetailsComponent } from './creat/travel-details/travel-details.component';
import { FlightOrTranComponent } from './creat/flight-or-tran/flight-or-tran.component';
import { AccomodationComponent } from './creat/accomodation/accomodation.component';
import { CarPoolComponent } from './creat/car-pool/car-pool.component';
import { TripService } from './trip.service';


@NgModule({
  declarations: [
    TripInformationComponent,
    TravelDetailsComponent,
    FlightOrTranComponent,
    AccomodationComponent,
    CarPoolComponent,
    CreateTripComponent,
  ],
  imports: [
    CommonModule,
    TripRoutingModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatStepperModule,
    TripService
  ]
})
export class TripModule { }
