import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Trip, masterData } from 'src/app/_models/trip';
import { TripService } from '../../trip.service';

@Component({
  selector: 'app-trip-information',
  templateUrl: './trip-information.component.html',
  styleUrls: ['./trip-information.component.css']
})
export class TripInformationComponent implements OnInit {
  newTrip: Trip;
  loading = false;
  submitted = false;
  returnUrl: string;
  tripCreat: FormGroup;

  arFromLocation: masterData;
  arToLocation: masterData;
  arPurpuse: masterData;
  arTripFor: masterData;
  arTripCostCenter: masterData;
  arEventCode: masterData;
  constructor(private formBuilder: FormBuilder, private tripService: TripService) {

    this.arFromLocation = this.tripService.arFromLocation;
    this.arToLocation = this.tripService.arToLocation;
    this.arPurpuse = this.tripService.arPurpuse;
    this.arTripFor = this.tripService.arTripFor;
    this.arTripCostCenter = this.tripService.arTripCostCenter;
    this.arEventCode = this.tripService.arEventCode;
   }

  ngOnInit(): void {

    this.tripCreat = this.formBuilder.group({
      travleType: ['Domestic Travel', Validators.required],
      tripType: ['One Way', Validators.required],
      fromLocation: [this.arFromLocation[2], Validators.required],
      toLocation: [this.arToLocation[1], Validators.required],
      startDate: [new Date(), Validators.required],
      endDate: [new Date(), Validators.required],
      purpuse: [this.arPurpuse[0], Validators.required],
      tripFor: [this.arTripFor[0], Validators.required],
      travelDetails: ['', Validators.required],
      tripCostCenter: [null, Validators.required],
      eventCode: [null, Validators.required]
  });
  }
  get f() { return this.tripCreat.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.tripCreat.invalid) {
        return;
    }
    console.log('submit', this.f);
  }


}
