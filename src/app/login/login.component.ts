import { Component, OnInit, OnDestroy } from '@angular/core';
import { GoogleLoginProvider, AuthService, SocialUser, SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { AuthenticationService } from '../_services/authentication.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../_models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  currentUser: User;
  currentUserSubscription: Subscription;
  private loggedIn: boolean;

  constructor(
    public OAuth: AuthService,  
    private authenticationService: AuthenticationService,  
    private router: Router
    ) { 
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user;
      });
    }

  ngOnInit(): void {
  }

  public socialSignIn(socialProvider: string) {  
    let socialPlatformProvider;  
    if (socialProvider === 'google') {  
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;  
    }  
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  
      console.log(socialusers);  
      this.login(socialusers);  
    });  
  } 
  login(user) {
    const getUser = this.authenticationService.login(user);
    this.router.navigate([`/home`]);
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
  }

}
